const hargaMakanan = [
    { nama: 'Sate', harga: 24000 },
    { nama: 'Nasi Goreng', harga: 17000 },
    { nama: 'Mie Rebus', harga: 14000 },
    { nama: 'Nasi Bakar', harga: 9000 },
    { nama: 'Tempe Goreng', harga: 4000 },
    { nama: 'Mie Goreng', harga: 17000 },
    { nama: 'Ikan Bakar', harga: 32000 },
    { nama: 'Nasi Uduk', harga: 6000 },
    { nama: 'Kerupuk', harga: 1000 },
    { nama: 'Lalapan', harga: 3000 },
  ];
  const totalMakanan = hargaMakanan[4].harga;
  console.log(totalMakanan);

  const totalMakanan = hargaMakanan[9].harga;
console.log(totalMakanan);

const totalMakanan = hargaMakanan.filter(makan => makan.harga >= 15000);
console.log(totalMakanan);

const totalMakanan = hargaMakanan.filter(makan => makan.harga <= 15000);
console.log(totalMakanan);

const totalMakanan = hargaMakanan.filter(a => a.harga <= 25000 && a.harga >= 10000);
console.log(totalMakanan);

const totalMakanan = hargaMakanan.sort((a, b) => a.harga - b.harga);
console.log(totalMakanan);

const totalMakanan = hargaMakanan.sort((a, b) => b.harga - a.harga);
console.log(totalMakanan);

const totalMakanan = hargaMakanan.find(a => a.harga==14000 );
console.log(totalMakanan);

const totalMakanan = hargaMakanan.find(a => a.harga==17000 );
console.log(totalMakanan);

const totalMakanan = hargaMakanan.filter(a => a.harga==17000 );
console.log(totalMakanan);

const totalMakanan = hargaMakanan.filter(a => a.nama.includes('Goreng'));
console.log(totalMakanan);

const totalMakanan = hargaMakanan.filter(a => a.nama.includes('Nasi'));
console.log(totalMakanan);


